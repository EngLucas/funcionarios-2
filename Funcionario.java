class Funcionario {
	private String nome;
	private	String empresa;
	private double salario;
	private	String rg;
	private	boolean trabalhando;
	Data entrada;


	public void setNome (String umNome){
		this.nome = umNome;
	}

	public String getNome(){
		return nome;
	}
	
	public void setEmpresa (String umaEmpresa){
		this.empresa = umaEmpresa;
	}

	public String getEmpresa(){
		return empresa;	
	}
	
	public void setSalario (double umSalario){
		this.salario = umSalario;
	}
	
	public double getSalario (){
		return salario;	
	}
	
	public void setRg (String umRG){
		this.rg = umRG;
	}

	public String getRg (){
		return rg;
	}
	
	public void setTrabalhando (boolean estaOuNao){
		this.trabalhando = estaOuNao;
	}
	
	public boolean getTrabalhando(){
		if (trabalhando == true){
			return true;
		}else 	
			return false;
	}
	
	public void bonifica (double umDinheiro){
		this.salario = this.salario + umDinheiro;
	}

	public void demitido (){
		this.trabalhando = false;
	}
	
	public void mostra (){
		System.out.println("\nNome: " + getNome());
		System.out.println("Empresa: " + getEmpresa());
		System.out.println("Salario: " + getSalario());
		System.out.println("RG: " + getRg());
		System.out.println("Data de Entrada: " + this.entrada.dia + "/" + this.entrada.mes + "/" + this.entrada.ano);
		System.out.println("Ativo na Empresa: " + getTrabalhando()+ "\n");
	
		
	
	}
	/* Comentario respondendo a questão 8: Não faz sentido colocar a class conta
	para fazer metodos pois ela nao é um objeto e nem referencia de um 
	ela é somente uma ideia para fazer os metodos é onde entra a ideia de criar
	um objeto, quando você usa os metodos você toma como referencia um objeto 
	para ele ser modificado ao longo do codigo.*/


}
